function getUsers() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Invalid address");
      }

      return response.json();
    })
    .then((data) => {
      let result = data;

      console.log(result);
    })
    .catch((error) => {
      console.log(error.message);
    });
}

getUsers();
