function getUsesrNameAndDetails() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Invalid address");
      }
      return response.json();
    })
    .then((data) => {
      const result = data.reduce((accumulator, current) => {
        let obj = {};

        obj.userName = current.name.split(" ").slice(0, 1).toString();

        obj.details = current;

        accumulator.push(obj);

        return accumulator;
      }, []);

      console.log(result);
    })
    .catch((error) => {
      console.log(error.message);
    });
}

getUsesrNameAndDetails();
