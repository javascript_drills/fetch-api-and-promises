function getUserData() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Invalid Api address");
      }
      return response.json();
    })
    .then((data) => {
      return Promise.all([
        data,
        fetch("https://jsonplaceholder.typicode.com/todos"),
      ]);
    })
    .then((response1) => {
      if (response1[1].status !== 200) {
        throw new Error("Invalid Api address");
      }
      return Promise.all([response1[0], response1[1].json()]);
    })
    .then((data) => {
      dataDelivery(data[0], data[1]);
    })
    .catch((error) => {
      console.log(error.message);
    });
}

getUserData();

function dataDelivery(usersData, usersTodo) {
  const usersDataBase = usersData.reduce((acc, curr) => {
    let obj = {};

    obj.user = curr.name;
    obj.id = curr.id;

    acc.push(obj);

    return acc;
  }, []);

  let usersTodos = {};

  for (let index = 0; index < usersTodo.length; index++) {
    let name = usersTodo[index]["userId"];
    let title = usersTodo[index]["title"];

    if (usersTodos[name] === undefined) {
      let temp = [];

      temp.push(title);

      usersTodos[name] = temp;
    } else {
      let t1 = usersTodos[name];

      t1.push(title);
      usersTodos[name] = t1;
    }
  }

  print(usersDataBase, usersTodos);
}

function print(usersDataBase, usersTodos) {
  for (let key of usersDataBase) {
    console.log(" * User Name : " + key.user);

    for (let key1 in usersTodos) {
      if (key1 == key.id) {
        usersTodos[key1].forEach((element) => {
          console.log(element);
        });
      }
    }
    console.log(" =====================================");
  }
}
