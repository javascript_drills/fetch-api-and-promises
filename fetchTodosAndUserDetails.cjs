function getTodoAndUserDetails() {
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => {
      if (response.status !== 200) {
        throw new Error("Invalid address");
      }
      return response.json();
    })
    .then((todo) => {
      return Promise.all([
        todo,
        fetch("https://jsonplaceholder.typicode.com/users"),
      ]);
    })
    .then((response) => {
      if (response[1].status !== 200) {
        throw new Error("Invalid address");
      }
      return Promise.all([response[0], response[1].json()]);
    })
    .then((data) => {
      fd(data[0], data[1]);
    })
    .catch((error) => {
      console.log(error.message);
    });
}

getTodoAndUserDetails();

function fd(todo, usersDetails) {
  const todoOne = todo.filter((obj) => obj.id === 1);

  let todosUseser = todoOne[0].userId;

  let title = todoOne[0].title;

  const finalData = usersDetails.filter((obj) => obj.id === todosUseser);

  let Object = {};

  Object.Title = title;

  Object.details = finalData;

  console.log(Object);
}
